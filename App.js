import React, { Component } from 'react';

import {
  COLOR,
  ThemeProvider,
  Button
} from 'react-native-material-ui';
import { TextField } from 'react-native-material-textfield';

import {
  Image,
  StyleSheet,
  View,
  Text
} from 'react-native';

import {
  Router,
  Stack,
  Scene
} from 'react-native-router-flux';

import { Actions } from 'react-native-router-flux';

import FitImage from 'react-native-fit-image';

import LoginView from './src/login.component';
import CameraView from './src/camera_view.component';
import PictureDetailView from './src/picture.component';

const uri = 'https://www.festisite.com/static/partylogo/img/logos/google.png';

export default class App extends Component<{}> {
  render() {
    return (
      <ThemeProvider uiTheme={uiTheme}>
        <Router>
          <Stack key="root">
            <Scene key="login" component={LoginView} title="Login" hideNavBar/>
            <Scene key="cameraView" component={CameraView} title="Camera" hideNavBar/>
            <Scene key="pictureView" component={PictureDetailView} title="Picture detail" hideNavBar/>
          </Stack>
        </Router>
      </ThemeProvider>
    );
  }
}

const uiTheme = {
    palette: {
        primaryColor: COLOR.blue500,
    },
    toolbar: {
        container: {
            height: 50,
        },
    },
    button: {
        container: {
          marginVertical: 20,
        }
    }
};
