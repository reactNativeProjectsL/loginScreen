import React, { Component } from 'react';

import {
  COLOR,
  ThemeProvider,
  Button
} from 'react-native-material-ui';

import {
  StyleSheet,
  Dimensions,
  View
} from 'react-native';

import FitImage from 'react-native-fit-image';

export default class PictureDetailView extends Component {
  constructor(props) {
      super(props);

      this.state = {
        uriImage:this.props.uriImage
      }

      console.warn(this.state);
  }

  render() {
    return (
      <ThemeProvider uiTheme={uiTheme}>
        <View style={styles.container}>
          <View style={styles.imageContainer}>
            <FitImage
              resizeMode="contain"
              source={{ uri:this.state.uriImage }}
              style={styles.imageLogo} />
          </View>
        </View>
      </ThemeProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  imageContainer: {
    alignSelf: 'stretch',
    flex: 1,
    marginHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageLogo: {
    height: 110,
    width: 300,
  }
});

const uiTheme = {
    palette: {
        primaryColor: COLOR.blue500,
    },
    toolbar: {
        container: {
            height: 50,
        },
    },
    button: {
        container: {
          marginVertical: 20,
        }
    }
};
