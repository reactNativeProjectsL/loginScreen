import React, { Component } from 'react';

import {
  COLOR,
  ThemeProvider,
  Button
} from 'react-native-material-ui';

import {
  StyleSheet,
  Text,
  Dimensions,
  View
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import Camera from 'react-native-camera';

export default class CameraView extends Component {
  render() {
    return (
      <ThemeProvider uiTheme={uiTheme}>
        <View style={styles.container}>
         <Camera
             ref={(cam) => {
               this.camera = cam;
             }}
             style={styles.preview}
             aspect={Camera.constants.Aspect.fill}>
             <Text style={styles.capture} onPress={this.takePicture.bind(this)}>[CAPTURE]</Text>
         </Camera>
        </View>
      </ThemeProvider>
    );
  }

  takePicture() {
   this.camera.capture()
     .then((data) => {
       Actions.pictureView({uriImage:data.mediaUri})
     })
     .catch(err => console.error(err));
 }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  preview: {
   flex: 1,
   justifyContent: 'flex-end',
   alignItems: 'center',
   height: Dimensions.get('window').height,
   width: Dimensions.get('window').width
 },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40
  }
});

const uiTheme = {
    palette: {
        primaryColor: COLOR.blue500,
    },
    toolbar: {
        container: {
            height: 50,
        },
    },
    button: {
        container: {
          marginVertical: 20,
        }
    }
};
