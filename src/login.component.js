import React, { Component } from 'react';

import {
  COLOR,
  ThemeProvider,
  Button
} from 'react-native-material-ui';

import { TextField } from 'react-native-material-textfield';

import {
  Image,
  StyleSheet,
  View,
  Text
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import FitImage from 'react-native-fit-image';

const uri = 'https://www.festisite.com/static/partylogo/img/logos/google.png';

export default class LoginView extends Component<{}> {

  constructor(props) {
      super(props);

      this.state = {
        phone:''
      }

      this.passwordRef = this.updateRef.bind(this, 'passwordField');
  }

  onSubmitEmail = () => {
    this.passwordField.focus();
  }

  _onPressedButton = () => {
    Actions.cameraView();
  }

  render() {
    return (
      <ThemeProvider uiTheme={uiTheme}>
        <View style={styles.container} >
          <View style={styles.imageContainer}>
            <FitImage
              resizeMode="contain"
              source={{uri:uri}}
              style={styles.imageLogo} />
          </View>
          <View style={styles.formContainer} >
            <View style={{ alignSelf: 'stretch' }}>
              <TextField
                onSubmitEditing={this.onSubmitEmail}
                tintColor='blue'
                baseColor='black'
                textColor='black'
                keyboardType='email-address'
                returnKeyType='next'
                label='Correo electrónico'
                onChangeText={ (phone) => this.setState({ phone }) }
                style={styles.textInput}
                />
              <TextField
                ref={this.passwordRef}
                tintColor='blue'
                baseColor='black'
                textColor='black'
                keyboardType='email-address'
                returnKeyType='done'
                autoCapitalize='none'
                autoCorrect={false}
                label='Password'
                onChangeText={ (password) => this.setState({ password }) }
                style={styles.textInput}
                />
              <Button
                style={styles.buttonSignIn}
                raised
                primary
                onPress={this._onPressedButton}
                text="Iniciar sesión" />
            </View>
          </View>
        </View>
      </ThemeProvider>
    );
  }

  updateRef(name, ref) {
    this[name] = ref;
  }
}

const uiTheme = {
    palette: {
        primaryColor: COLOR.blue500,
    },
    toolbar: {
        container: {
            height: 50,
        },
    },
    button: {
        container: {
          marginVertical: 20,
        }
    }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
    backgroundColor: '#F5FCFF',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  formContainer: {
    alignSelf: 'stretch',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 20,
  },
  imageContainer: {
    alignSelf: 'stretch',
    flex: 1,
    marginHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageLogo: {
    height: 110,
    width: 300,
  }
});
